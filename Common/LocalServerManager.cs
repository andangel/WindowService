﻿using System;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using WindowServices.Common.Properties;
using WindowServices.Server.Interface;

namespace WindowServices.Common
{
    public class LocalServer : IServer
    {
        #region IServer
        readonly ServiceControllerEx _serviceController = new ServiceControllerEx("CommonPlugService");

        public void Start()
        {
            _serviceController.Refresh();
            if (_serviceController.Status == ServiceControllerStatus.Stopped)
            {
                _serviceController.Start();
            }
        }

        public void Stop()
        {
            _serviceController.Refresh();
            if (_serviceController.Status == ServiceControllerStatus.Running)
            {
                _serviceController.Stop();
            }
        }

        public ServerState GetServiceState()
        {
            _serviceController.Refresh();
            return (ServerState)(int)_serviceController.Status;
        }


        public void SendCustomCommand(int command, ServiceEntityEx serviceEntity, byte[] fileContent)
        {
            switch ((ServiceCommand)command)
            {
                case ServiceCommand.Add:
                case ServiceCommand.Update:
                    if (!Directory.Exists(serviceEntity.ZipPath))
                    {
                        Directory.CreateDirectory(serviceEntity.ZipPath);
                    }

                    File.WriteAllBytes(serviceEntity.ZipFileName, fileContent);
                    using (var config = new TempServiceConfig(true))
                    {
                        config.AddServiceEntity(serviceEntity);
                    }
                    break;
                case ServiceCommand.Delete:
                    using (var config = new TempServiceConfig(true))
                    {
                        config.AddServiceEntity(serviceEntity);
                    }
                    break;
            }

            _serviceController.Refresh();
            _serviceController.ExecuteCommand(command);
        }


        public void Pause()
        {
            _serviceController.Refresh();
            if (_serviceController.Status == ServiceControllerStatus.Running)
            {
                _serviceController.Pause();
            }
        }

        public void Continue()
        {
            _serviceController.Refresh();
            if (_serviceController.Status == ServiceControllerStatus.Paused)
            {
                _serviceController.Continue();
            }
        }



        public System.Collections.Generic.List<ServiceEntityEx> GetServiceList()
        {
            return ServiceHelper.GetAllServiceEntity();
        }

        #endregion


        public void WaitForCommplete()
        {
            WaitForCommplete(120);
        }

        public void WaitForCommplete(int timeOut)
        {
            if (timeOut <= 0)
            {
                throw new Exception(Resources.WaitTimeIsZero);
            }
            var count = timeOut;
            while (File.Exists(TempServiceConfig.ConfigFile) && count > 0)
            {
                count--;
                Thread.Sleep(1000);
            }
        }
    }
}
