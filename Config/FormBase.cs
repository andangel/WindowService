﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;

namespace WindowServices.Config
{
    public static class FreshUi
    {
        static readonly Dictionary<Type, IProcess> Dic = new Dictionary<Type, IProcess>();
        static FreshUi()
        {
            Dic.Add(typeof(Form), new FormProcess());
            Dic.Add(typeof(Label), new LabelProcess());
            Dic.Add(typeof(Button), new ButtonProcess());
            Dic.Add(typeof(DataGridView),new DataGridViewProcess());
            Dic.Add(typeof(CheckBox), new CheckBoxProcess());
        }
// ReSharper disable once InconsistentNaming
        internal static void FreshUIControl(Control c)
        {
            if (c is Form)
            {
                Dic[typeof(Form)].ProcessControl(c);
            }
            else
            {
                if (c is UserControl)
                { }
                else
                {
                    if (Dic.ContainsKey(c.GetType())) Dic[c.GetType()].ProcessControl(c);
                }
            } foreach (Control control in c.Controls)
            {
                FreshUIControl(control);
            }
        }
        internal static T GetResourceObject<T>(string name, string preFix)
        {
            name = FixName(name, preFix);
            var pi = typeof(Properties.Resources).GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static); if (pi != null) { var ob = pi.GetValue(null, null); if (ob != null) { if (ob is T) { return (T)ob; } } } return default(T);
        }
        private static string FixName(string name, string preFix)
        {
            if (preFix != "")
            {
                name = name.Replace(preFix, "");
            } return name;
        }
    }
    public abstract class Process<T> : IProcess where T : Control
    {
        public void ProcessControl(Object t)
        {
            if (t == null)
            {
                return;
            } 
            
            ProcessT((T)t);
        }
        public abstract void ProcessT(T t);
    }
    public interface IProcess
    {
        void ProcessControl(object t);
    }
    public class FormProcess : Process<Form>
    {
        #region IProcess<Form> 
        public override void ProcessT(Form f)
        {
            f.Text = FreshUi.GetResourceObject<string>(f.Name, "");
        }
        #endregion
    }
    public class LabelProcess : Process<Label>
    {
        #region IProcess<Label> 
        public override void ProcessT(Label t)
        {
            t.Text = FreshUi.GetResourceObject<string>(t.Name, "lbl");
        }
        #endregion
    }
    public class ButtonProcess : Process<Button>
    {
        #region IProcess<Button> 
        public override void ProcessT(Button t)
        {
            t.Text = FreshUi.GetResourceObject<string>(t.Name, "btn");
        }
        #endregion
    }

    public class  DataGridViewProcess:Process<DataGridView>
    {

        public override void ProcessT(DataGridView t)
        {
            foreach (DataGridViewColumn column in t.Columns)
            {
                column.HeaderText = FreshUi.GetResourceObject<string>(column.Name, "col");
            }
        }
    }

    public class CheckBoxProcess : Process<CheckBox>
    {
        #region IProcess<CheckBox>
        public override void ProcessT(CheckBox t)
        {
            t.Text = FreshUi.GetResourceObject<string>(t.Name, "chk");
        }
        #endregion
    }
}
