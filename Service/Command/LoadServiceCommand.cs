using System.Collections.Generic;
using WindowServices.Common;
using WindowServices.Server.Interface;
using WindowServices.Service.Command.Task;
using WindowServices.Service.Properties;

namespace WindowServices.Service.Command
{
    internal sealed class LoadServiceCommand : ICommand
    {
        private readonly List<ServiceEntityEx> _list;
        private readonly Dictionary<string, ServiceHelper> _services;
        public LoadServiceCommand(Dictionary<string, ServiceHelper> services, List<ServiceEntityEx> list)
        {
            _services = services;
            _list = list;
        }

        public void Execute()
        {
            LogHelper.WriteEntityServiceInfo(Resources.StartService,EventType.Info);
            _list.ForEach(c =>
                              {
                                  var sh = Utility.LoadService(c);
                                  if (sh == null)
                                  {
                                      //LogHelper.WriteEntityServiceError(string.Format("���ط���{0}��ʧ��", c.Name), EventType.StartService);
                                      return;
                                  }
                                  _services.Add(c.Key, sh);
                              });
            LogHelper.WriteEntityServiceInfo(Resources.StartServiceSuccess, EventType.Info);
        }

        public void RollBack()
        {

        }
    }
}