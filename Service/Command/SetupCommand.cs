using System.Collections.Generic;
using WindowServices.Server.Interface;
using WindowServices.Service.Command.Task;

namespace WindowServices.Service.Command
{
    internal sealed class SetupCommand : CommandBase
    {
        public SetupCommand(Dictionary<string, ServiceHelper> services, ServiceEntityEx se)
            : base(new List<ITask>
                       {
                           new UnZipFileTask(se, 1), 
                           new LoadServiceTask(services, se, 2), 
                           new SaveConfigTask(se, 3)
                       })
        {
        }
    }
}