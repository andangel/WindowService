using WindowServices.Server.Interface;

namespace WindowServices.Service.Command.Task
{
    internal sealed class UnZipFileTask : TaskBase
    {
        public UnZipFileTask(ServiceEntityEx se)
            : base(se)
        {
        }

        public UnZipFileTask(ServiceEntityEx se, int order)
            : base(se, order)
        {
        }

        protected override void RollBack(ServiceEntityEx se)
        {
            Utility.DeleteServiceDir(se);
        }

        protected override void Execute(ServiceEntityEx se)
        {
            Utility.UnZipFile(se);
        }
    }
}