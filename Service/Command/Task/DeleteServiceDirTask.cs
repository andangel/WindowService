using System.Threading;
using WindowServices.Common;
using WindowServices.Server.Interface;
using WindowServices.Service.Properties;

namespace WindowServices.Service.Command.Task
{
    internal sealed class DeleteServiceDirTask : TaskBase
    {
        public DeleteServiceDirTask(ServiceEntityEx se, int order)
            : base(se, order)
        {
        }

        public DeleteServiceDirTask(ServiceEntityEx se)
            : base(se)
        {
        }
        protected override void RollBack(ServiceEntityEx se)
        {
            Utility.UnZipFile(se);
        }

        protected override void Execute(ServiceEntityEx se)
        {
            LogHelper.WriteEntityServiceInfo(string.Format(Resources.DelayDeleteDirectory, se.BasePath), EventType.Info);
            Thread.Sleep(30000);
            Utility.DeleteServiceDir(se);
        }
    }
}