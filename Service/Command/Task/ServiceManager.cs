using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using WindowServices.Common;
using WindowServices.Server.Interface;
using WindowServices.Service.Properties;

namespace WindowServices.Service.Command.Task
{
    internal class ServiceManager //: IServerManager
    {

        readonly Dictionary<string, ServiceHelper> _services = new Dictionary<string, ServiceHelper>();

        public void Setup(ServiceEntityEx se)
        {
            if (Directory.Exists(se.BasePath))
            {
                throw new Exception(string.Format(Resources.DirectoryIsExists, se.BasePath));
            }

            var list = new List<ITask> { new UnZipFileTask(se, 1), new LoadServiceTask(_services, se, 2), new SaveConfigTask(se, 3) };
            list.Sort((c1, c2) => c1.Order.CompareTo(c2.Order));

            try
            {
                list.ForEach(c => c.Execute());
            }
            catch (Exception)
            {
                list.Reverse();
                list.ForEach(c => c.RollBack());
                throw;
            }


        }

        public void Update(ServiceEntityEx se)
        {

        }

        public void UnInstall(ServiceEntityEx se)
        {
            if (!_services.ContainsKey(se.Key))
            {
                return;
            }
            int step = 0;
            ServiceHelper sh = _services[se.Key];
            try
            {
                _services.Remove(se.Key);
                step = 1;
                UNload(sh);
                step = 2;

                LogHelper.WriteEntityServiceInfo("延时30秒后删除服务目录，请稍候....", EventType.Info);
                Thread.Sleep(30000);
                DeleteServiceEntityDir(se);
                step = 4;
                Common.ServiceHelper.DelServiceEntity(se.Id, se.Version);
                step = 8;

            }
            catch (Exception)
            {
                if ((step & 4) != 0)
                {
                    Directory.CreateDirectory(se.BasePath);
                    ZipHelper.UnZip(se.ZipFileName, se.BasePath + "\\", null);
                }

                if ((step & 2) != 0)
                {
                    sh = CreateService(se);
                }

                if ((step & 1) != 0)
                {
                    _services.Add(se.Key, sh);
                }
                throw;
            }
        }

        private static ServiceHelper CreateService(ServiceEntityEx se)
        {
            LogHelper.WriteEntityServiceInfo("加载服务【" + se.Name + "】开始....", EventType.Info);

            try
            {
                var sh = ServiceHelper.CreateServiceHelper(se);
                if (sh != null)
                {

                    sh.Initialize(se);
                    sh.Start();
                    LogHelper.WriteEntityServiceInfo("加载服务【" + se.Name + "】完成。", EventType.Info);
                    return sh;
                }

                LogHelper.WriteEntityServiceError("加载服务【" + se.Name + "】失败。", EventType.CreateServiceError);
            }
            catch (Exception ee)
            {
                LogHelper.WriteEntityServiceError(ee.ToString(), EventType.StartServiceError);
            }

            return null;
        }
        delegate void StopDelegate();
        private static void UNload(ServiceHelper sh)
        {
            if (sh == null)
            {
                return;
            }

            string serviceName = sh.ServiceName;
            LogHelper.WriteEntityServiceInfo("正在停止服务【" + serviceName + "】...", EventType.Info);

            var sd = new StopDelegate(sh.Stop);
            sd.BeginInvoke(null, null);
            Thread.Sleep(5000);
            sh.Dispose();
            AppDomain.Unload(sh.CurrentDomain);

            LogHelper.WriteEntityServiceInfo("正在停止服务【" + serviceName + "】完成", EventType.Info);
        }

        private static void DeleteServiceEntityDir(ServiceEntityEx se)
        {
            LogHelper.WriteEntityServiceInfo("延时30秒后删除服务目录，请稍候....", EventType.Info);
            Thread.Sleep(30000);

            try
            {
                LogHelper.WriteEntityServiceError(string.Format("删除目录【{0}】开始...", se.BasePath), EventType.Info);
                Directory.Delete(se.BasePath, true);
                LogHelper.WriteEntityServiceError(string.Format("删除目录【{0}】成功.", se.BasePath), EventType.Info);
            }
            catch (Exception exception)
            {
                LogHelper.WriteEntityServiceError(string.Format("删除目录【{0}】出错.\n错误信息：{1}", se.BasePath, exception), EventType.DeletePlugPathError);
            }
        }
        private void LoadService(IEnumerable<ServiceEntityEx> list)
        {
            foreach (var se in list)
            {
                ServiceHelper sh = CreateService(se);
                if (sh != null)
                {
                    _services.Add(se.Key, sh);
                }
            }
        }

        public void StartAllService()
        {
            var list = Common.ServiceHelper.GetAllServiceEntity();
            if (list == null)
            {
                LogHelper.WriteEntityServiceError("读取配置文件出错，请检查配置文件是否正确.", EventType.ReadConfigError);
                return;
            }

            LoadService(list);
        }

        public void StopAllService()
        {
            foreach (var key in _services.Keys)
            {
                LogHelper.WriteEntityServiceInfo("停止服务开始....", EventType.Info);
                UNload(_services[key]);
                LogHelper.WriteEntityServiceInfo("停止服务完成", EventType.Info);
            }

            _services.Clear();
        }
    }

    #region 命令接口

    #endregion
}