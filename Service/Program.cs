﻿using System;
using System.Globalization;
using System.ServiceProcess;
using System.Threading;
using WindowServices.Common;
using WindowServices.Service.Properties;

namespace WindowServices.Service
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainUnhandledException;
            if(System.Configuration.ConfigurationManager.AppSettings["Language"] != null)
            {
                Resources.Culture = new CultureInfo(System.Configuration.ConfigurationManager.AppSettings["Language"]);
            }

            Common.Properties.Resources.Culture = Resources.Culture;
#if(!DEBUG1)
            var servicesToRun = new ServiceBase[] 
                                              { 
                                                  new Services() 
                                              };
            ServiceBase.Run(servicesToRun);
#else
            new Services().OnStart(null);
            //System.Console.ReadKey();
#endif
        }

        static void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            LogHelper.WriteEntityServiceError(string.Format("IsTerminating:{0},ExceptionObject:{1}", e.IsTerminating, e.ExceptionObject), EventType.RunServiceError);
        }
    }
}
