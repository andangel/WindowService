using System;

namespace WindowServices.Server.Interface
{
    [Serializable]
    public enum ServiceState
    {
        Add = 1,
        Edit,
        Del,
        None
    }

    [Serializable]
    public enum ServerState
    {

        /// <summary>
        /// 服务即将继续。这对应于 Win32 SERVICE_CONTINUE_PENDING 常数，该常数定义为 0x00000005。 
        /// </summary>
        ContinuePending = 0x00000005,

        /// <summary>
        ///服务已暂停。这对应于 Win32 SERVICE_PAUSED 常数，该常数定义为 0x00000007。
        /// </summary>
        Paused = 0x00000007,
        /// <summary>
        ///服务即将暂停。这对应于 Win32 SERVICE_PAUSE_PENDING 常数，该常数定义为 0x00000006。  
        /// </summary>
        PausePending = 0x00000006,
        /// <summary>
        ///服务正在运行。这对应于 Win32 SERVICE_RUNNING 常数，该常数定义为 0x00000004。
        /// </summary>
        Running = 0x00000004, 
        /// <summary>
        ///服务正在启动。这对应于 Win32 SERVICE_START_PENDING 常数，该常数定义为 0x00000002。 
        /// </summary>
        StartPending = 0x00000002,

        /// <summary>
        ///服务未运行。这对应于 Win32 SERVICE_STOPPED 常数，该常数定义为 0x00000001。 
        /// </summary>
        Stopped = 0x00000001,

        /// <summary>
        /// 服务正在停止。这对应于 Win32 SERVICE_STOP_PENDING 常数，该常数定义为 0x00000003。
        /// </summary>
        StopPending = 0x00000003,
    }
}