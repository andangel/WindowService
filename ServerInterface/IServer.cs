
using System.Collections.Generic;
using System.ServiceModel;

namespace WindowServices.Server.Interface
{
    [ServiceContract]
    public interface IServer
    {
        [OperationContract]
        void Start();
        [OperationContract]
        void Stop();
        [OperationContract]
        void SendCustomCommand(int command, ServiceEntityEx serviceEntity, byte[] fileContent);
        [OperationContract(Name = "WaitForComplete120")]
        void WaitForCommplete();
        [OperationContract]
        void WaitForCommplete(int timeOut);
        [OperationContract]
        ServerState GetServiceState();
        [OperationContract]
        void Pause();
        [OperationContract]
        void Continue();
        [OperationContract]
        List<ServiceEntityEx> GetServiceList();
    }
}