﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Threading;
using LogServiceHelper;
using WindowServices.Interface;

namespace LogService
{
    [Description("LogService.")]
    [DisplayName("LogService")]
    [Guid("777FD1A6-9D73-4835-8514-402C2ACBE739")]
    [Serializable]
    public class Service : IService
    {
        #region IService Members

        private ServiceEntity _se;

        public void Initialize(ServiceEntity serviceEntity)
        {
            _se = serviceEntity;
        }

        private bool _isRunning;

        public void Start()
        {
            _isRunning = true;
            while (_isRunning)
            {
                try
                {
                    LogHelper.Logger.InfoFormat("ID={0},Name={1},Type={2},Version={3}", _se.Id, _se.Name, _se.Type,
                                                     _se.Version);
                }
                catch(Exception exception)
                {
                    LogHelper.Logger.Error("写入日志出错。",exception);
                }

                Thread.Sleep(5000);
            }
        }

        public void Stop()
        {
            _isRunning = false;
        }

        public void Pause()
        {
        }

        #endregion
    }
}
