﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WindowServices.Interface
{
    /// <summary>
    /// 服务基本程序
    /// 扩展基本的键值扩展配置读取
    /// </summary>
    public abstract class ServiceBase:IService
    {
        public void Initialize(ServiceEntity serviceEntity)
        {
            throw new NotImplementedException();
        }

        public abstract void Start();

        public abstract void Stop();

        public abstract void Pause();
    }
}
